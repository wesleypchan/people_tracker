# install dependencies for spencer_people_tracking package

#!/bin/sh

echo "Install required libraries..."
# install libraries                                                                                                                                                                    
sudo apt-get install libsvm-dev qt4-dev-tools

echo "Downloading and install opencv2..."
# install openvc2

CD=$PWD
cd ~
wget https://github.com/opencv/opencv/archive/2.4.13.6.zip
unzip 2.4.13.6.zip
cd opencv-2.4.13.6
mkdir build
cd build
cmake ..
make
sudo make install
cd ../..
rm 2.4.13.6.zip
cd $CD

# create symbolic link of required library                                                                                                                                             
sudo ln -s /usr/local/cuda/lib64/libcudart.so /usr/lib/libopencv_dep_cudart.so

# required by spencer group tracking
sudo apt-get install python-scipy ros-`rosversion -d`-leg-detector

echo "Done"
