This package contains the configuration launch files for using the [spencer people tracker](https://github.com/spencer-project/spencer_people_tracking) with kinect and realsense cameras. 

It mainly uses topic relays to give spencer people tracker the input topics it needs in its own namespace. 

This package also provides the functionality of recording and playing back compressed files. 

### Installation  ###

- run the install-dependencies.sh script to download and install the dependencies of the spencer tracker including opencv 2
- install the [spencer_people_tracking pacakge](https://github.com/spencer-project/spencer_people_tracking)

### Running the people tracker ###
## With Kinect  ##
    $ roslaunch people_tracker tracking_kinect_sick.launch camera:=camera laser:=true ## use the laser arg to turn on/off laser detector
    $ rosrun rviz rviz -d $(rospack find people_tracker)/rviz/tracking_camera_link.rviz ## runs visualization in rviz

## With realsense camera ##
    $ roslaunch people_tracker tracking_realsense2_velodyne.launch camera:=camera laser:=true ## use the laser arg to turn on/off laser detector
    $ rosrun rviz rviz -d $(rospack find people_tracker)/rviz/tracking_camera_link.rviz ## runs visualization in rviz

### Recording and playing back bag files ###
    # to record
    $ rosrun people_tracker recordBagCompressed.sh 'filename' ## edit the file to change the save folder location
    # to play back
    $ roslaunch people_tracker tracking_on_bag.launch bagfile:='filename' ## edit the launch file to change the bag file folder location

## Relayed topics ##
- /spencer/sensors/rgbd_front_top/depth/camera_info : depth camera info topic
- /spencer/sensors/rgbd_front_top/depth/image_rect : depth camera rectified image
- /spencer/sensors/rgbd_front_top/rgb/camera_info : rgb camera info topic
- /spencer/sensors/rgbd_front_top/rgb/image_rect_color : rgb camera rectified color image
- /spencer/sensors/laser_front/echo : laser scan topic
