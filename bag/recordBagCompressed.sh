# filename="/media/usb/bags/chris.internal.us-west1.0x78.xyz/people_tracker/$1.bag"
folder="/home/caris/experiment/people_tracker/"
file="$folder/$1.bag"
mkdir $folder
rosbag record -O $file \
       /spencer/sensors/rgbd_front_top/depth/camera_info \
       /spencer/sensors/rgbd_front_top/depth/image_raw/compressedDepth \
       /spencer/sensors/rgbd_front_top/rgb/camera_info \
       /spencer/sensors/rgbd_front_top/rgb/image_raw/compressed \
       /tf \
       /tf_static \
       
