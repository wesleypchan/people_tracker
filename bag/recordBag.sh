# filename="/media/usb/bags/chris.internal.us-west1.0x78.xyz/people_tracker/$1.bag"
filename="/home/caris/experiment/people_tracker/$1.bag"
rosbag record -O $filename \
       /scan \
       /tf \
       /tf_static \
       /right_realsense/depth/camera_info \
       /right_realsense/depth/image_rect \
       /right_realsense/rgb/camera_info \
       /right_realsense/rgb/image_rect_color \
       /right_realsense/rgb/image_raw \
       /right_realsense/depth_registered/points

